<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
    <title>PHP 101</title>
</head>
<body>
	<!--Indique la date et l'heure du serveur-->
	<?php
	setlocale(LC_TIME, 'fr_FR.utf8','fra');
	echo 'Nous sommes le ', '<strong> ', (strftime("%A %d %B")), '</strong>', ' et il est ', '<strong>', (strftime("%X")), '</strong>';
	?>

	<!--EXERCICE 1-->
	<h1>Exercice 1</h1>
	<form method="POST">
		<input name="name" type="text" placeholder="Entrez votre prénom">
		<input name="button" type="submit" value="OK">
	</form>
	<?php 
	$name = $_POST["name"]; //récupère la donnée de l'input
	echo '</br>';
	echo '<em>', ($name), '</em>', ' ', 'Votre nom est de ', (strlen($name)), ' caractères.' ;
	?>
	<?php 
	if (strlen($name) <= 10 && strlen($name) >= 1){
		echo "<h3> Bienvenue $name !</h3>";
	}
	else if (strlen($name) > 10){ 
		echo "<h3>Votre nom est beaucoup trop long. Changez de nom. Immédiatement.</h3>";
	}
	else{
		echo "<h3>Votre nom n'est pas valide. C'est dommage.</h3>";} 
	?>


	<!--EXERCICE 2-->
	<h1>Exercice 2</h1>
	<form method="POST">
		<input name="login" type="text" placeholder="Enter your login">
		<input name="mdp" type="password" placeholder="Enter your password">
		<input name="button" type="submit" value="Ok">
	</form>
	<?php
	$username = $_POST["login"];
	$password = $_POST["mdp"];
	echo($username);
	echo($password);
	//Vérifie que le login fasse plus de 4 caractères et contient @
	if (strlen($username) < 4 || stristr($username, '@') === FALSE ){ 
		echo "<h3> Invalid login - ex : something@iueuiuiuie.mail</h3>";
	}
	//vérifie les données exactes pour l'authentification
	else if ($username === "lea@gmail.com" && $password == "abE353_5"){
		echo "<h3>Authentification réussie</h3>";
	}
	else if ($username != "lea@gmail.com"){
		echo "<h3>Wrong login</h3>";
	}
	else if ($password != "abE353_5"){
		echo "<h3>Wrong password</h3>";
	}
	//indique à l'utilisateur qu'il n'est pas enregistré dans la base de données
	else {
		echo "<h3>You're not signed in</h3>";
	}
	?>
	<!--EXERCICE 3 -->
	<h1>Exercice 3 - Fight the calculette</h1>
	<?php
	//Générer les nombres et l'opérateur aléatoires
	$nombre1 = random_int(0, 100);
	$nombre2 = random_int(0, 100);
	$operateur = ["x", "+", "-", "/"];
	$operateurRand = array_rand($operateur, 1);
	echo '<h3> Quel est le résultat de ', ($nombre1), ' ', ($operateurRand[$i]), ' ', ($nombre2), ' ?</h3>';
	?>
	<form method="POST">
		<input type="number" name="reponseUtilisateur" require>
		<input type="submit" value="Valider">
	</form>
	<?php
	$reponseUtilisateur = $_POST['reponseUtilisateur'];
	//Déterminer le résultat de l'opération
	if($operateur = $operateur[0]){
		$resultat = $nombre1 * $nombre2;
	}
	else if ($operateur = $operateur[1]){
		$resultat = $nombre1 + $nombre2;
	}
	else if ($operateur = $operateur[2]){
		$resultat = $nombre1 - $nombre2;
	}
	else if ($operateur = $operateur[3]){
		$resultat = $nombre1 / $nombre2;
	}
	echo($resultat);
	
	//Vérifier la réponse de l'utilisateur
	if($reponseUtilisateur == $resultat){
		echo '<h3>Vous avez gagné, bravo ! </h3>';
	}
	else { echo '<h3>Dommage, votre princesse est dans un autre château. </h3>'; }
	?>

	<!--EXERCICE 4 -->
	<h1>Exercice 4 - Moyenne</h1>

	<h3>Veuillez saisir les notes</h3>
	<form method="post" action="." id="exercice4">
		<label for="notes">Note 1</label>
		<input type="number" name="notes[0]" id="note1" min="0" max="20" required>
		<label for="notes">Note 2</label>
		<input type="number" name="notes[1]" id="note2" min="0" max="20" required>
		<label for="notes">Note 3</label>
		<input type="number" name="notes[2]" id="note3" min="0" max="20" required>
		<label for="notes">Note 4</label>
		<input type="number" name="notes[3]" id="note4" min="0" max="20" required>
		<label for="notes">Note 5</label>
		<input type="number" name="notes[4]" id="note5" min="0" max="20" required>
		<label for="notes">Note 6</label>
		<input type="number" name="notes[5]" id="note6" min="0" max="20" required>
		<label for="notes">Note 7</label>
		<input type="number" name="notes[6]" id="note7" min="0" max="20" required>
		<label for="notes">Note 8</label>
		<input type="number" name="notes[7]" id="note8" min="0" max="20" required>
		<label for="notes">Note 9</label>
		<input type="number" name="notes[8]" id="note9" min="0" max="20" required>
		<label for="notes">Note 10</label>
		<input type="number" name="notes[9]" id="note10" min="0" max="20" required>
		<input type="submit" value="Calculer la moyenne">
		<input type="reset" value="Réinitialiser les notes">
	</form>

	<?php
	//Créé un tableau qui récupère les notes saisies
	$notes = ($_POST['notes']);
	//Affiche les valeurs du tableau
	echo 'Voici les notes :', implode(",", $notes);
	//Fais la somme des notes et la divise par le nombre de notes
	$moyenne = array_sum($notes) / count($notes);
	echo '<h3>Voici la moyenne : </h3>', '<h3>', ($moyenne), '</h3>';
	?>

	<!--EXERCICE 5 -->
	<h1>Exercice 5 - Nombre mystère</h1>
	<?php
	$reponse = $_POST['devine'];
	$valeurMystere = $_POST['aleatoire'];
	//Générer un nombre aléatoire stocké dans un input caché
	if(is_null($valeurMystere)){
		$aleatoire = random_int(1, 10);
	}
	else {
		$aleatoire = $valeurMystere;
	}
	?>
	<!--Formulaire HTML -->
	<form action="." method="post">
		<label for="devine">Devinez quel nombre j'ai choisi <em>(entre 1 et 10)</em></label>
		<input type="number" placeholder="Nombre mystère ?" name="devine">
		<input type="hidden" name="aleatoire" value="<?=$aleatoire?>">
		<input type="submit" value="Deviner" name="bouton">
	</form>

	<!-- Vérifier la réponse de l'utilisateur -->
	<?php
	if ($reponse == $aleatoire){
		echo '</br>Bravo t\'es trop fort!';
		//Génère un nouveau nombre aléatoire
		$aleatoire = random_int(1, 10);
	}
	else if ($reponse < $aleatoire){
		echo '</br>Raté ! Mon chiffre est plus grand : ', $aleatoire;
	}
	else if ($reponse > $aleatoire){
		echo '</br>Raté ! Mon chiffre était plus petit : ', $aleatoire;
	}
	?>
</body>
</html>