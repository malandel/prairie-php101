## Exercice 1

* Demander le prénom
* Vérifier que le prénom fait entre 1 et 10 caractères
* Afficher un message du type "Bonjour [prénom] !"

## Exercice 2

* Créer un formulaire d'authentification par identifiant et mot de passe
* L'identifiant doit contenir plus de 4 caractères, et un arobase
* L'identifiant doit être "lea@gmail.com"
* Le mot de passe doit être "abE353_5"
* Un message doit dire si l'authentification a réussit ou non

## Exercice 3

* Le programme tire aléatoirement une opération (+, -, x) et deux chiffres
* Le programme demande à l'utilisateur la réponse
* Puis dit si le résultat est bon ou faux !

## Exercice 4

* Demander à l'utilisateur de saisir 10 notes
* Afficher en retour la moyenne

## Exercice 5

* Le programme tire un nombre aléatoirement entre 1 et 10
* L'utilisateur donne un nombre
* Le programme répond "plus", "moins", ou "gagné !"
* Le programme tourne en boucle jusqu'à ce que l'utilisateur ait trouvé la bonne valeur
